from platform import system as system_name # Returns the system/OS name
import subprocess as sp
import os
import logging
__version__=1.0

def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that some hosts may not respond to a ping request even if the host name is valid.
    """
    with open(os.devnull, 'w') as DEVNULL:

       # Ping parameters as function of OS
       parameters = "-n 1" if system_name().lower()=="windows" else "-c 1"
       status, result = sp.getstatusoutput("ping "+parameters + " "+ host)

       # Pinging
       return [(status == 0) , result]

import requests
import re

def getMyExtIp():
    res=None
    try:
        res = requests.get("http://whatismyip.org")
        myIp = re.compile('(\d{1,3}\.){3}\d{1,3}').search(res.text).group()
        if myIp != "":
            return myIp
    except:
        return "failed to get IP address from http://whatismyip.org. "+res.text
        pass
    return "n/a"


import argparse
def checkIfUp(logger,address):
    [result, text] = ping(address)

    if (result):
        logger.info(address + " OK.")
    else:
        logger.info(address + " IS NOT REACHABLE. " + text)
    return result
def logIpAddy(logger):
    ipaddy=getMyExtIp()
    logger.info("External IP address: " +ipaddy)

import threading,time
import datetime
import csv
def main(argv):

    downTime=None  #records the first time it went down

    address = 'rdl1-l101.bng.optusnet.com.au'

    logger = logging.getLogger('internet uptime')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(address+'.log')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)
    logger.info("************************************Uptime Monitor STARTED************************************")
    count=0
    sleeptime=5
    checkTime=1   #multiples of sleeptime
    ipCheckTime=10 #multiples of sleeptime
    down=False
    #logIpAddy(logger)

    while True:
        time.sleep(sleeptime)

        if(count%checkTime==0):
            if(checkIfUp(logger,address)):  #Site is up
                if down :  #if it was down previously then
                    logIpAddy(logger)
                    down=False
                    #now that it is back up then get the time difference it was down for.
                    nowTime=datetime.datetime.now()
                    dif=nowTime-downTime
                    dif=divmod(dif.days * 86400 + dif.seconds, 60)
                    m=str(dif[0])
                    s=str(dif[1])

                    text='\n----------------------------\n'
                    text+='Down Between: '+str(downTime) +' - '+str(nowTime) +'\n'
                    text+='Down for : '+m+'min '+ str(s)+'s'+'\n----------------------------'
                    logger.info(text)
                    with open(address+'_downtime.csv','a',newline='') as csvfile:
                        writer=csv.writer(csvfile,delimiter=',')
                        writer.writerow([str(downTime),str(nowTime),(m+'m,'+s+'s')])



                    downTime=None

            else:
                down=True
                if downTime==None:
                    downTime = datetime.datetime.now()  # records the first time it went down

        if (count % ipCheckTime == 0):
            if not down:  #no point in checking for ipaddress if down
                logIpAddy(logger)


        count+=1



import sys
if __name__ == "__main__":
    main(sys.argv)


